# Super Server

The [Super Server] exposes a simple API in order to store and report
active game servers.

The exposed data is used by the [Launcher] and other instances.

::: warning temporary
This project is meant as a quick alternative to the actual Super Server from the [ZCAPI]. 
It will be active until we got a working subset of the [ZCAPI].
:::

## how it works

All super servers are added to the [Super Server List]. Clients (~Launcher) and other
super servers can use this list to retrieve active super servers.

The super server will sync itself with other super servers and provide json endpoints.

## endpoints

- `GET /`: retrieve a list of active game server instances
- `POST /`: add a game server instance
- `DELETE /`: remove a game server instance
- `GET /sync`: synchronize game servers with other super servers

[Super Server]: https://gitlab.com/blrevive/super-server
[ZCAPI]: /dev/backend/zcapi/index
[Super Server List]: https://
[Launcher]: /dev/frontend/launcher/index