# SDK Generator <Badge>C++</Badge> <Badge>UE3</Badge>

Generates a C++ SDK for Blacklight: Retribution with an API for all UObjects and various tools like a detouring interface.

The purpose is to make reversing and modification of the game managable.



::: warning Very limited currently
The current implementation has several **big** downsides which is why the code is rewritten from scratch.
See [Rewrite](#rewrite) for details.
:::

## Usage

You have to inject the `SdkGenerator_<version>.dll` into a running instance of Blacklight: Retribution with the same `<version>`.

After injection you may start the generation process at any time by pressing `F6`.

::: warning grab a coffe
The generation will take a few minutes but will prompt when finished so don't panic and don't exit the game!
:::

The generated SDK can be found at the game folder inside the `SDK_<version>` directory.

## How it works

The Unreal Engine (>=3) provides a lightweight reflection system which allows us to traverse all [`UObject`]s in the game.

#### UE3 object system summary

- [`UObject`] is the base class of everything related to UE3 inside the game - only native code doesn't inherit it.
- Every UObject belongs to an [`FName`] and has a unique index (`ObjectInternalInteger`). 
- An [`FName`] is just a wrapper for ascii strings that are used inside the game.
- The engine also provides a global [`TArray`] of all objects (`GObjects`) and names (`GNames`) loaded. 


### generating the sdk

The generator iterates through the `TArray<UObject*>* GObjects` in order to find all [`UClass`]es and
construct C++ equivalents of the classes.

[`UObject`]: https://gitlab.com/blrevive/tools/blr-sdk-generator/-/blob/v3.02/GameDefines.h#L278
[`FName`]: https://gitlab.com/blrevive/tools/blr-sdk-generator/-/blob/v3.02/GameDefines.h#L171
[`TArray`]: https://gitlab.com/blrevive/tools/blr-sdk-generator/-/blob/v3.02/GameDefines.h#L121
[`UClass`]: https://gitlab.com/blrevive/tools/blr-sdk-generator/-/blob/v3.02/GameDefines.h#L462

## Rewrite

Due to the limitations, which make development realy hard, the SDK generator is currently rewritten from scratch.

### issues of current implementation

- you have to include all classes at once to overcome missing dependencies (due to missing forward declerations and header seperation)
- you can only include the header once for the whole project because of missing header guards
- alot of redundant code is generated

### goal

- provide a version independant core sdk (which also is used to generate the complete sdk)
- provide clean code structure
    - seperate header files for every class
    - logic implemented in cpp files
- provide plain C structs of classes for import into Ghidra/xDbg
- detouring helpers (VTable for VFunctions and JMP for native functions)

### workflow
The sdk generator consists of 2 components: **Aggregator** and **Serializer**.
The diagram below illustrates the structure and workflow of those.
![SdkGenWorkflow](./SdkGen.svg)

#### Aggregator <Badge>C++</Badge> [<Badge>:link: Proxy-Module</Badge>](/dev/tools/proxy/#modules)

Collects information about unreal classes and structures from a specific client and 
saves them to a json file (called object database).

::: warning json references
The output file is not meant to be human readable and uses alot of json references
to minimize disk space and get rid of recursions.
:::

#### Serializer <Badge>C#</Badge>

Parses the object database and serializes the artifacts to 

- a full C++ SDK including API for all `UObjects`, event listeners, native hooks..
- a C API containing plain c structs (no polymorphism or inheritance) that can be imported into Ghidra



[Core SDK]: #
