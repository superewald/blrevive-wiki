# Game Server Parameters

## BLRevive Parameters

These are the parameters that are added from our side in order to manage your server and provide it to other users.

| Param          | Description                                    | Example                      |
| -------------- | ---------------------------------------------- | ---------------------------- |
| `Title` <Badge type="warning">required</Badge>       | Name of the server                             | `Awesome BLR Server`         |
| `Description`  | A little description of the server             | `Metro DM 24/7`              |
| `Game Client`  <Badge type="warning">required</Badge> | The client which is used to start the server   | `3.02`                       |
| `Region`       <Badge type="warning">required</Badge> | The region your server is located at           | `EU`                         |
| `Address` <Badge>auto</Badge>      | Your public IPv4 Address   | `92.241.24.42`               |
| `Port`      <Badge type="warning">required</Badge>   | The port the game server willr un on | `7777` |

## Game Settings

| Param          | Description                                    | Example                      |
| -------------- | ---------------------------------------------- | ---------------------------- |
| `Playlist`     | Server playlist, will overwrite map & gamemode | `KC`               |
| `Game Mode`    | Game mode to start                             | `DM` |
| `Map`          | The initial map                                | `Metro` |
| `NumBots`      | Amount of bots in the server                   | `4`                 |
| `MaxPlayers`   | Maximum amount of players on the server        | `16`              |

## Additional Parameters

You can also provide additional parameters to the server (for flags just leave value empty).

Here's a quick list of currently known parameters, for more look at [Developers/Reverse Engineering/CLI Parameters]().

| Param          | Description                                    | Example                      |
| -------------- | ---------------------------------------------- | ---------------------------- |
| `ServerName`   | Name of the server                             | `?ServerName="cat"`          |
| `TimeLimit`    | Time limit of the server in minutes            | `?TimeLimit=100`             |
| `NumBots`      | Amount of bots in the server                   | `?NumBots=4`                 |
| `Port`         | Internal port the server will start on         | `?Port=7778`                 |
| `Playlist`     | Server playlist, will overwrite map & gamemode | `?Playlist=DM`               |
| `MaxPlayers`   | Maximum amount of players on the server        | `?MaxPlayers=8`              |
| `GamePassword` | Optional password required to join server      | `?GamePassword="cat123"`     |
| `GameSpeed`    | Modifier for the game speed                    | `?GameSpeed=1.5`             |
| `Game`         | Gamemode                                       | `?Game=FoxGame.FoxGameMP_DM` |
| `SCP`          | Starting CP                                    | `?SCP=2000`                  |

::: warning GameSpeed is bugged.
On first spawn everything will be affected by the `GameSpeed` (like you'd expect it to) but after respawning for the first time almost everything client side will go back to normal. HRV recharge and nade cooking time for example is still affected by GameSpeed, run speed however is not. It's therefore not recommended to use the `GameSpeed` parameter on a server.
:::

### Working Playlists

This is a list of playlists known to currently work with the `Playlist` parameter

| Gamemode           | Playlist ID | Comment            |
| ------------------ | ----------- | ------------------ |
| Deathmatch         | DM          |                    |
| Kill Confirmed     | KC          |                    |
| Team Deathmatch    | TDM         |                    |
| Last Man Standing  | LMS         |                    |
| Last Team Standing | LTS         |                    |
| Capture The Flag   | CTF         |                    |
| King of the Hill   | KOTH        |                    |
| Domination         | DOM         |                    |
| Search and Destroy | SND         |                    |
| Onslaught          | OS_Medium   | Gamemode is bugged |