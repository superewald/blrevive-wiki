# Super Server

The super server is responsible for providing a list of running game servers.

Its current implementation is a dead simple PHP script which will expose a light api for registering game servers
and synchronizing its storage with other super servers.

## Setup

You can find the setup & usage instructions in the repository [BLRevive/Super Server](https://gitlab.com/blrevive/super-server).