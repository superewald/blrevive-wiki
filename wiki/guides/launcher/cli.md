# Launcher CLI

::: warning Expiremental
The CLI is currently in expiremental stage, which means it may not work appropriately.
When implementation is finished this page will be updated

**Until then use the help command: `Launcher.exe help` to list all commands**
:::

## Registry

Manage game clients.

### Add Client

### Remove Client

## Patcher

Patch game clients.

`BLRevive.exe patch <ClientID>`

## Launcher

### Start Client

### Start Server

## Status Server