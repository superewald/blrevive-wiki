# Getting Started :video_game:

Welcome to the world of BLRevive, a place for all Blacklight refugees!

::: tip Discord
Make sure to join our [Discord] if you haven't allready for recent updates, discussions
and help about this project.
:::


## Quickstart

1. Download [Blacklight: Retribution v3.02](https://steamdb.info/app/209870/)
1. Download the latest [Launcher](https://blrevive.superewald.net/releases/BLRevive-0.4-Preview-5.exe)
1. [Setup the Launcher](#launcher-setup)
1. Start play!

::: warning Limitations!
There are currently a few limitations due to the ongoin development.
Most notable the **customization** and **loadouts**  are missing and the user is forced to the default loadout (SMG, AR, SR).
:::

## Launcher Setup

In order to play BL:R again you first have to setup the launcher!

### Add BL:R client

You need to provide at least one BL:R client (v3.02/v3.01) to the launcher to be able to play.

- open the launcher
- go to `Settings`
- drag & drop the `<BLR>/Binaries/Win32/FoxGame-win32-Shipping.exe` into the `Game Clients` grid <InlinePreview>![AddGameClientDragDrop](./AddBlrClient.mp4)</InlinePreview>
::: warning not available on Linux currently
Avalonia has currently no/limited support for drag & drop on linux which is why you need to this instead:

- click on `Add`  beside the `Game Clients` grid
- click on `Browser`  *or* enter path to `<BLR>/Binaries/Win32/FoxGame-win32-Shipping.exe`
- click `Save` to add the client
:::


### Patch BL:R client

After adding the client you also need to apply our patches.

- goto `Settings`
- select unpatched client
- click on `Patch` <InlinePreview>![PatchBlrClient](./PatchBlrClient.mp4)</InlinePreview>

::: warning indicator stays red
There is a bug with the patch indicator (the red/green box) which shows still red (unpatched)
after a succesfull patch. After restarting the launcher it will update properly!
([BLRevive/Launcher #6](https://gitlab.com/blrevive/launcher/-/issues/6))
:::

### Connect to Game Servers (Joining a Game)

You can either use our server browser which gives you easy access to running game servers or connect to a specific server by address.

#### Server Browser

- go to `Server Browser`
- right-click on the server list and chose `Refresh`
- right-click on a server entry in the server list and chose `Connect->3.02`
<InlinePreview>
![UseServerBrowser](./UseServerBrowser.mp4)
</InlinePreview>



#### Server Address

- go to `Server Browser`
- at the `Custom Server` section enter the IP and Port of the game server
- click on `Connect`

After connecting to a server the client will start.

### FAQ

Before starting to seek help in our [Discord] make sure it isn't covered by this FAQ!

::: faq Client crashes when loading
Most likely you forgot to patch the client. If you did and it does still crash, seek help in our [Discord] or open an issue.
:::


::: faq "Can't connect to steam"
Your client failed to connect to the server. Either try a different server or report it to the server owner.
:::

[Discord]: https://discord.gg/wwj9unRvtN