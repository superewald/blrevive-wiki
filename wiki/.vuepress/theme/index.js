module.exports = {
    extend: '@vuepress/theme-default',
    plugins: [
        ['container', {
            type: 'faq',
            before: info => `<details class="details faq"><summary>${info}</summary>`,
            after: () => `</details>`
        }]
    ]
}